import java.util.Scanner;
class Calc
{
	int number1,number2,y,ch;
	Scanner in=new Scanner (System.in);
	void input()
	{
		System.out.println("Enter first number : ");
		number1=in.nextInt();
		System.out.println("Enter second number : ");
		number2=in.nextInt();		
	}
	int addition()
	{
		int result;
		result=number1+number2;
		return result;
	}
	int subtraction()
	{
		int result;
		result=number1-number2;
		return result;
	}
	int product()
	{
		int result;
		result=number1*number2;
		return result;
	}
	int quotient()
	{
		int result;
		result=number1/number2;
		return result;
	}
	int remainder()
	{
		int result;
		result=number1%number2;
		return result;
	}
	void check()
	{
		System.out.println("Enter choice : ");
		ch=in.nextInt();
		switch(ch)
		{
			case 1:	
				y=addition();		
				System.out.println("Addition is "+y);
				break;
			case 2: 
				y=subtraction();		
				System.out.println("Subtraction is "+y);
				break;
			case 3: 
				y=product();		
				System.out.println("Product is "+y);
				break;
			case 4: 
				y=quotient();		
				System.out.println("Quotient is "+y);
				break;
			case 5: 
				y=remainder();		
				System.out.println("Remainder is "+y);
				break;
			default: 
				System.out.println("Invalid Input");
				break;
		}

	}
	public static void main(String [] ar)
	{
		Calc ob=new Calc();
		System.out.println("MENU ");
		System.out.println("1. Addition ");
		System.out.println("2. Subtraction ");
		System.out.println("3. Product ");
		System.out.println("4. Quotient ");
		System.out.println("5. Remainder ");
		ob.input();
		ob.check();
	}
}